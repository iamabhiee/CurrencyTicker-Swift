//
//  DashboardViewController.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 22/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet var tableView : UITableView!
    @IBOutlet var defaultCurrencyButton : UIBarButtonItem!
    @IBOutlet var favoriteButton : UIBarButtonItem!
    
    let favoriteSegueName = "showFavorites"
    let defaultCurrencySelectionSegueName = "showDefaultCurrency"
    let detailsSegueName = "showDetails"
    
    var currencyExchangeData : [CurrencyExchange] = []
    
    lazy var dataManager = CurrencyDataManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.registerNib(DashboardTableViewCell.NibObject(), forCellReuseIdentifier: DashboardTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        
        //Update Default currency
        updateBaseCurrencyTitle()
        reloadExchangeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! {
        case favoriteSegueName, defaultCurrencySelectionSegueName:
            configureCurrencySelectionController(segue.destinationViewController, segueIdentifier: segue.identifier!)
            break
            
        case detailsSegueName:
            let currentCurrencyExchangeData = currencyExchangeData[(tableView.indexPathForSelectedRow?.row)!]
            let detailsController = segue.destinationViewController as! ExchangeDetailsViewController
            detailsController.selectedCurrencyExchange = currentCurrencyExchangeData
        default:
            break
        }
    }
    
    // MARK: - IBAction
    @IBAction func changeDefaultCurrency(sender: UIButton) {
        performSegueWithIdentifier(defaultCurrencySelectionSegueName, sender: sender)
    }
    
    @IBAction func changeFavoriteCurrencies(sender: UIButton? = nil) {
        performSegueWithIdentifier(favoriteSegueName, sender: sender)
    }
    
    // MARK: - Custom methods
    func updateBaseCurrencyTitle() {
        let defaultCurrency = dataManager.defaultCurrency
        let defaultCurrencyTitle = String.init(format: "%@ ▿", defaultCurrency.code)
        defaultCurrencyButton.title = defaultCurrencyTitle
    }
    
    func reloadExchangeData() {
        currencyExchangeData = []
        
        let favoriteCurrencies = dataManager.favCurrencies
        let defaultCurrency = dataManager.defaultCurrency
        
        if favoriteCurrencies.isEmpty {
            let placeholderView = PlaceholderView.instanceFromNib()
            placeholderView.configure("Oops, You need to select some currencies to see some data here :(", buttonTitle: "Select Currencies")
            placeholderView.delegate = self
            self.tableView.backgroundView = placeholderView
            self.tableView.reloadData()
            return
        } else {
            self.tableView.backgroundView = nil
        }
        
        fetchExchangeData(favoriteCurrencies, defaultCurrency: defaultCurrency)
    }
    
    func fetchExchangeData(favoriteCurrencies : [Currency], defaultCurrency : Currency) {
        showProgressHud()
        ExchangeDataManager.fetchLatestExchangeDataWithDifference(favoriteCurrencies, baseCurrency: defaultCurrency) {result, error in
            self.hideProgressHud()
            if let responseError = error {
                self.handleError(responseError)
            } else {
                if let responseExchageData = result {
                    self.currencyExchangeData = responseExchageData
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func configureCurrencySelectionController(controller : UIViewController, segueIdentifier : String) {
        let navController = controller as! UINavigationController
        let currencySelectionController = navController.viewControllers.first as! CurrencySelectionViewController
        let allCurrencies = dataManager.allCurrencies
        currencySelectionController.currencies = allCurrencies
        currencySelectionController.delegate = self
        
        if segueIdentifier == defaultCurrencySelectionSegueName {
            currencySelectionController.selectedCurrencies = [dataManager.defaultCurrency]
            currencySelectionController.mode = .DefaultCurrencySelection
        } else if segueIdentifier == favoriteSegueName {
            currencySelectionController.selectedCurrencies = dataManager.favCurrencies
            currencySelectionController.mode = .FavoriteCurrencySelection
        }
        
    }
}

extension DashboardViewController : UITableViewDataSource {
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyExchangeData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(DashboardTableViewCell.identifier) as! DashboardTableViewCell
        let currentCurrencyExchangeData = currencyExchangeData[indexPath.row]
        cell.configureWithExchangeData(currentCurrencyExchangeData)
        return cell
    }
}

extension DashboardViewController : UITableViewDelegate {
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier(detailsSegueName, sender: self)
    }
}

extension DashboardViewController : CurrencySelectionControllerDelegate {
    func didSelectCurrencies(currencies: [Currency], sender: CurrencySelectionViewController) {
        if sender.mode == .DefaultCurrencySelection {
            if let firstCurrency = currencies.first {
                dataManager.defaultCurrency = firstCurrency
                updateBaseCurrencyTitle()
            }
        } else if sender.mode == .FavoriteCurrencySelection {
            dataManager.favCurrencies = currencies
        }
        
        let defaultCurrency = dataManager.defaultCurrency;
        if dataManager.favCurrencies.contains(defaultCurrency) {
            dataManager.favCurrencies.removeObject(defaultCurrency)
        }
        
        reloadExchangeData()
    }
}

extension DashboardViewController : PlaceholderViewDelegate {
    func didTapPlaceholderActionButton() {
        changeFavoriteCurrencies()
    }
}
