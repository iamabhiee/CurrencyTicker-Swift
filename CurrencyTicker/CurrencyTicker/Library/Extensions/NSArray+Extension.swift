//
//  NSArray+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 19/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

extension Array where Element : Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}
