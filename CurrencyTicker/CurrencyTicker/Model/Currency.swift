//
//  Currency.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class Currency : NSObject {
    var name : String!
    var code : String!
    var symbol : String!
    
    override init() {
        
    }
    
    init?(arrayData : [String]?) {
        if let data = arrayData where arrayData?.count == 3 {
            code = data[0]
            name = data[1]
            symbol = data[2]
        } else {
            return nil
        }
    }
    
    override var description : String {
        return "Name : \(name)" +
        "Code : \(code)" +
        "Symbol : \(symbol)"
    }
    
    required init(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObjectForKey("name") as! String
        code = aDecoder.decodeObjectForKey("code") as! String
        symbol = aDecoder.decodeObjectForKey("symbol") as! String
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(code, forKey: "code")
        aCoder.encodeObject(symbol, forKey: "symbol")
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let otherObject = object as? Currency {
            return self.name == otherObject.name && self.code == otherObject.code && self.symbol == otherObject.symbol
        }
        return false
    }
}

