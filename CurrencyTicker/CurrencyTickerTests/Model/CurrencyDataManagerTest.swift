//
//  CurrencyDataManagerTest.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 29/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import XCTest

@testable import CurrencyTicker

class CurrencyDataManagerTest: XCTestCase {
    
    var sut : FakeCurrencyDataManager!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sut = FakeCurrencyDataManager()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        sut = nil
    }
    
    func testInit_ShouldNotBeNil() {
        XCTAssertNotNil(sut, "CurrencyDataManager should not be nil")
    }
    
    func testInitAllCurrency_ShouldNotBeEmpty() {
        XCTAssertFalse(sut.allCurrencies.isEmpty, "All Currencies should not be empty")
    }
    
    func testDefaultCurrency_ShouldNotBeNil() {
        let defaultCurrency = sut.defaultCurrency
        
        XCTAssertNotNil(defaultCurrency, "Default Currency should not be empty")
        XCTAssertNotNil(defaultCurrency.name, "Default Currency should not be empty")
        XCTAssertNotNil(defaultCurrency.code, "Default Currency should not be empty")
        XCTAssertNotNil(defaultCurrency.symbol, "Default Currency should not be empty")
    }
    
    func testDefaultCurrencyChange_ShouldBeSame() {
        let currency = sut.currencyForCode("USD")
        sut.defaultCurrency = currency
        
        XCTAssertEqual(currency, sut.defaultCurrency)
    }
    
    func testDefaultCurrencyChangeTwice_ShouldNotBeSame() {
        let currency = sut.currencyForCode("USD")
        sut.defaultCurrency = currency
        
        let newCurrency = sut.currencyForCode("INR")
        sut.defaultCurrency = newCurrency
        
        XCTAssertNotEqual(currency, sut.defaultCurrency)
    }
    
    func testFavoriteCurrencyToggle_ShouldUpdateCount() {
        let count = sut.favCurrencies.count
        
        sut.toggleFavoriteForCurrencyAtIndex(0)
        let newCount = sut.favCurrencies.count
        
        XCTAssertEqual(count + 1, newCount, "New count should be increased by one")
        
        sut.toggleFavoriteForCurrencyAtIndex(0)
        let updateCount = sut.favCurrencies.count
        
        XCTAssertEqual(count, updateCount, "New count should be same with original count")
    }
    
    func testCurrencyAtIndex_ShouldReturnSameCurrency() {
        let firstCurrency = sut.allCurrencies.first
        let firstCurrencyFromMethod = sut.currencyAtIndex(0)
        
        XCTAssertEqual(firstCurrency, firstCurrencyFromMethod, "Both currencies should be the same")
    }
    
    func testIsCurrencySelected_ShouldChangeSelection() {
        //Its unselected
        let index = 0
        sut.toggleFavoriteForCurrencyAtIndex(index)
        let firstCurrencyFromMethod = sut.currencyAtIndex(index)
        
        //Select
        var isFavorite = sut.isCurrencySelected(firstCurrencyFromMethod)
        XCTAssertTrue(isFavorite)
        
        //Unselect
        sut.toggleFavoriteForCurrencyAtIndex(index)
        isFavorite = sut.isCurrencySelected(firstCurrencyFromMethod)
        XCTAssertFalse(isFavorite)
    }
    
    func testCurrencyForCode_ShouldReturnCurrencyForCode() {
        //Its unselected
        let currencyCode = "USD"
        let currency = sut.currencyForCode(currencyCode)
        XCTAssertEqual(currency.code, currencyCode, "Should get right currency")
    }
}

extension CurrencyDataManagerTest {
    class FakeCurrencyDataManager : CurrencyDataManager {
        override func toggleFavoriteForCurrencyAtIndex(index: Int) {
            let currency = currencyAtIndex(index)
            
            if favCurrencies.contains(currency) {
                favCurrencies.removeObject(currency)
            } else {
                favCurrencies.append(currency)
            }
        }
    }
}
