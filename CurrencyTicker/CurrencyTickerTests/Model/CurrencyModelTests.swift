//
//  CurrencyModelTests.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 19/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import XCTest

@testable import CurrencyTicker

class CurrencyModelTests: XCTestCase {
    
    let currencyName = "United States Dollar"
    let currencyCode = "USD"
    let currencySymbol = "$"
    
    let secondCurrencyName = "Indian Rupee"
    let secondCurrencyCode = "INR"
    let secondCurrencySymbol = "₹"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInitWithEmptyArray_ShouldBeNil() {
        let currencyData : [String] = []
        let currency = Currency(arrayData: currencyData)
        
        XCTAssertNil(currency, "Currency should be nil")
    }

    func testInitWithInvalidArray_ShouldBeNil() {
        let currencyData = [currencyName, currencyCode]
        let currency = Currency(arrayData: currencyData)
        
        XCTAssertNil(currency, "Currency should be nil")
    }
    
    func testInitWithNil_ShouldBeNil() {
        let currency = Currency(arrayData: nil)
        
        XCTAssertNil(currency, "Currency should be nil")
    }
    
    func testInitWithArray_ShouldInit() {
        let currencyData = [currencyCode, currencyName, currencySymbol]
        let currency = Currency(arrayData: currencyData)
        
        if let currency = currency {
            XCTAssertEqual(currency.name, currencyName, "Name should be equal")
            XCTAssertEqual(currency.code, currencyCode, "Code should be equal")
            XCTAssertEqual(currency.symbol, currencySymbol, "Symbol should be equal")
        } else {
            XCTAssertNotNil(currency, "Currency should not be nil")
        }
    }
    
    func testInitWithCoder_ShouldPass() {
        let currencyData = [currencyName, currencyCode, currencySymbol]
        let currency = Currency(arrayData: currencyData)!
        let currencyArray = [currency]
        
        let currenciesArchivedData = NSKeyedArchiver.archivedDataWithRootObject(currencyArray)
        let currenciesUnarchivedArray = NSKeyedUnarchiver.unarchiveObjectWithData(currenciesArchivedData) as! [Currency]
        
        XCTAssertEqual(currencyArray.count, currenciesUnarchivedArray.count, "Name should be equal")
    }
    
    func testEqual_ShouldBeSame() {
        let firstCurrencyData = [currencyName, currencyCode, currencySymbol]
        let firstCurrency = Currency(arrayData: firstCurrencyData)!
        let secondCurrency = Currency(arrayData: firstCurrencyData)!
        
        XCTAssertEqual(firstCurrency, secondCurrency, "Both currency should be equal")
    }
    
    func testNotEqual_ShouldNotBeSame() {
        let firstCurrencyData = [currencyName, currencyCode, currencySymbol]
        let firstCurrency = Currency(arrayData: firstCurrencyData)!
        
        let secondCurrencyData = [secondCurrencyName, secondCurrencyCode, secondCurrencySymbol]
        let secondCurrency = Currency(arrayData: secondCurrencyData)!
        
        XCTAssertNotEqual(firstCurrency, secondCurrency, "Both currency should not be equal")
    }
}

extension CurrencyModelTests {
    func firstCurrencyModel() -> Currency {
        let firstCurrencyData = [currencyName, currencyCode, currencySymbol]
        let firstCurrency = Currency(arrayData: firstCurrencyData)!
        
        return firstCurrency
    }
    
    func secondCurrencyModel() -> Currency {
        let currencyData = [secondCurrencyName, secondCurrencyCode, secondCurrencySymbol]
        let currency = Currency(arrayData: currencyData)!
        
        return currency
    }
}
